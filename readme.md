# Overview
This package contains the launch files, which allow you to simulate the Gen3_lite kinova arm in a MoveIt and Gazebo environment, and the python scripts which calculates the inverse kinematics, end effector speed and wich allow you also to move the arm, using scale speed functionnality.

The `scan_area.py` node contains an algorithm, that will allow the arm to scan a flat surface automatically by calculating
    - the normal vector of the plan,
    - the angle which keeps the arm in a perpendicular orientation to the plan
    - the number of trips needed to cover the entire area (plan),
without forgetting that during the execution of the trajectory the arm must maintain a certain distance 
between him and the surface

NOTE: To calculate the normal vector, we declare the coordinates of the four points of the plan, then by calling the `vector_compute` function, which is in `vector_util.py` script we calculate the normal vector and the Euler angles of the `end_effector`.

# Usage

# Step 1 
Go to `catkin_ws/src/` directory, and clone the following repositories :
    'https://github.com/Kinovarobotics/ros_kortex.git' 
and 
    'https://gitlab.com/a.abdellah/kinova_ptr.git'

# Step 2 
## Using Gazebo and Moveit
Using Gazebo! with Kinova Kortex Robots (Gen3_lite)
Before launching the gazebo world with the arm, go to the `kinova_ptr/worlds/kinova.Word` file, and please be sure that you have already chosen one of the three plans; vertical, horizontal or tilted, because in the next steps, we will use the coordinates of the chosen plan.
Then to launch the arm with default arguments, run the following command : 
    `roslaunch kinova_ptr gen3_lite_gazebo.launch`

Then launch the Moveit core API, run the following command : 
    `roslaunch kinova_ptr gen3_lite_move_group.launch`

## Using only Moveit
Using MoveIt! with Kinova Kortex Robots (Gen3_lite)
To launch it with default arguments, run the following command : 
    `roslaunch gen3_lite_gen3_lite_2f_move_it_config demo.launch `
Then refer directly to `Step 3`


# Step 3
## ->->-USING IK!!-<-<-
Using (Gen3_lite) with an automatic angle calculator, scale speed and inverse kinematic calculation functionality.
Before starting the node, please declare the coordinates correspond to the plan we want to scan, or directly uncomment the coordinates which are ready to be used in `scan_area.py` node.
You have to choose between the three plans; vertical, horizontal and tilted.
In case you want to use one of these three plans, be sure you choose the one that corresponds to the plan that you have already used in the `kinova_ptr/worlds/kinova.Word` file, then set the argument `graphic_representation` to true if you want the result to be graphed.

then run the following command : 
    `rosrun kinova_ptr scan_area.py`

# Step 4 
OPTIONAL: To calculate the end_effector speed with Kinova Kortex Robots (Gen3_lite), 
we use scale speed functionality, to start the node, run the following command : 
    `rosrun kinova_ptr speed_compute.py`




