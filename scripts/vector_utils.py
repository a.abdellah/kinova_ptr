import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
from numpy import (array, dot, arccos, clip)
from numpy.linalg import norm

def vector_compute(self, reference, p1, p2, p3, graphic_state):
    # These two vectors are in the plane
    self.vector1 = p2 - p1
    self.vector2 = p3 - p1
    self.reference = reference

    # the cross product is a vector normal to the plane
    self.n_vector = np.cross(self.vector1, self.vector2)

    angle_between_vectors(self, self.reference, self.n_vector)

    if graphic_state == True:
        plot_vectors(vector1, vector2, n_vector)

def angle_between_vectors(self, reference_axis, n_vctr):
    # -> cosine of the yaw and pitch angles
    c_y = dot(n_vctr,reference_axis[0])/norm(n_vctr)/norm(reference_axis[0])
    c_p = dot(n_vctr,reference_axis[1])/norm(n_vctr)/norm(reference_axis[1])
    # here we use clip function to specify the output interval
    self.yaw = arccos(clip(c_y, -2, 2))
    self.pitch = arccos(clip(c_p, -2, 2))

def plot_vectors(vct1, vct2, n_vct):
    vcts = np.array([[0, 0, 0, vct1[0], vct1[1], vct1[2]], [0, 0, 0, vct2[0], vct2[1], vct2[2]], [0, 0, 0, n_vct[0], n_vct[1], n_vct[2]]])
    #vcts = np.array([[0, 0, 0, vct1[0], vct1[1], vct1[2]], [0, 0, 0, vct2[0], vct2[1], vct2[2]]])
    X, Y, Z, U, V, W = zip(*vcts)
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.quiver(X, Y, Z, U, V, W)
    ax.set_xlim([-3, 3])
    ax.set_ylim([-3, 3])
    ax.set_zlim([-3, 3])
    plt.show()