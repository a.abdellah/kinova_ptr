#!/usr/bin/env python

import rospy, sys
import moveit_commander
import time
from moveit_msgs.msg import RobotTrajectory
from trajectory_msgs.msg import JointTrajectoryPoint
from math import *
from std_msgs.msg import Float64

from geometry_msgs.msg import PoseStamped, Pose
from tf.transformations import euler_from_quaternion, quaternion_from_euler

class MoveItDemo():
    def __init__(self):
        self.t1 = 0.0
        self.x1 = 0.0
        self.y1 = 0.0
        self.z1 = 0.0
        self.speed = rospy.Publisher("/end_effector_speed", Float64, queue_size=10, latch=True)
        moveit_commander.roscpp_initialize(sys.argv)
        arm = moveit_commander.MoveGroupCommander('arm')
        end_effector_link = arm.get_end_effector_link()
     
        #moveit_commander.roscpp_shutdown()
        #moveit_commander.os._exit(0)
        while not rospy.is_shutdown():
            current_pose = arm.get_current_pose(end_effector_link).pose
            x = current_pose.position.x
            y = current_pose.position.y
            z = current_pose.position.z
            self.compute(x, y, z)
            
    def compute(self, x2, y2, z2):
        t2 = time.time()
        Vx = (x2 - self.x1) / (t2 - self.t1)
        Vy = (y2 - self.y1) / (t2 - self.t1)
        Vz = (z2 - self.z1) / (t2 - self.t1)
        end_effector_speed = sqrt(Vx ** 2 + Vy ** 2 + Vz ** 2)
        print "The end_effector_speed is : " + str(end_effector_speed)
        self.speed.publish(end_effector_speed)
        self.x1 = x2
        self.y1 = y2
        self.z1 = z2
        self.t1 = t2
        rospy.sleep(0.1)  

    
    def listener(self):
        rospy.spin()
if __name__ == '__main__':
    rospy.init_node('MoveItDemo', anonymous=True)
    controlleur = MoveItDemo()
    controlleur.listener()


