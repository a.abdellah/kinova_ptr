#!/usr/bin/env python

import rospy, sys
import moveit_commander
from moveit_msgs.msg import RobotTrajectory
from trajectory_msgs.msg import JointTrajectoryPoint
from geometry_msgs.msg import PoseStamped, Pose
from tf.transformations import euler_from_quaternion, quaternion_from_euler
import numpy as np
from numpy import (array, dot, arccos, clip, cos, sin, pi)
from numpy.linalg import norm

from vector_utils import *
from scale_speed import *

class MoveItDemo:
    def __init__(self):
        # Initialize the move_group API
        moveit_commander.roscpp_initialize(sys.argv)
        rospy.init_node('moveit_demo')

        # Initialize the move group for the right arm
        self.arm = moveit_commander.MoveGroupCommander('arm')

        # Get the name of the end-effector link
        self.end_effector_link = self.arm.get_end_effector_link()

        # Get the current pose so we can add it as a waypoint
        current_pose = self.arm.get_current_pose(self.end_effector_link).pose  

        # Set the reference frame for pose targets
        self.reference_frame = 'base_link'

        # Set the right arm reference frame accordingly
        self.arm.set_pose_reference_frame(self.reference_frame)    

        # Allow replanning to increase the odds of a solution
        self.arm.allow_replanning(True)

        # Allow some leeway in position (meters) and orientation (radians)
        self.arm.set_goal_position_tolerance(0.01)
        self.arm.set_goal_orientation_tolerance(0.05)

        # set true if you want the result to be graphed
        graphic_representation = False

        # define absolute repere
        repere = np.array([[0, 1, 0], [0, 0, 1]])

        #############################################################################
        # define the points coordinates of the plan to compute the normal           #
        # vector we need just 3 points coordinates so we will only need to          #
        # use two vectors which are not collinear and which belongs to this plane   #
                                                                                    #
        ###### horizontal plan ######                                               #
        # self.pointB = np.array([0.4, -0.15, 0.0])                                 #
        # self.pointA = np.array([0.4, 0.15, 0.0])                                  #
        # self.pointD = np.array([0.2, 0.15, 0.0])                                  #
        # self.pointC = np.array([0.2, -0.15, 0.0])                                 #
                                                                                    #
        ###### vertical plan ######                                                 #
        self.pointB = np.array([0.7, -0.15, 0.3])                                   #
        self.pointA = np.array([0.7, 0.15, 0.3])                                    #
        self.pointD = np.array([0.7, 0.15, 0.1])                                    #
        self.pointC = np.array([0.7, -0.15, 0.1])                                   #
                                                                                    #
        ###### tilted  plan (0.78 rad) ######                                       #
        # self.pointB = np.array([0.69, -0.15, 0.17])                               #
        # self.pointA = np.array([0.69, 0.15, 0.17])                                #
        # self.pointD = np.array([0.55, 0.15, 0.03])                                #
        # self.pointC = np.array([0.55, -0.15, 0.03])                               #
        #############################################################################
                
        # vector_compute function will calculate the normal vector
        vector_compute(self, repere, self.pointB, self.pointA, self.pointD, graphic_representation)

        # angle between normal vector and z axis
        pitch1 = pi - self.pitch

        # convert euler angles to quaternion
        euler_Original = np.array([0.0, pitch1, 0.0 ])
        self.quaternion = quaternion_from_euler(euler_Original[0], euler_Original[1], euler_Original[2])

        self.target_pose = PoseStamped()
        self.target_pose.header.frame_id = self.reference_frame
        self.target_pose.header.stamp = rospy.Time.now()

        # home pose
        self.home_pose = [0.3, 0.0, 0.2, 0.0, 0.707106781187, 0.0, 0.707106781187]
        print("execute home pose")
        self.target_Pose(self.home_pose[0], self.home_pose[1], self.home_pose[2], self.home_pose[3], 
            self.home_pose[4], self.home_pose[5], self.home_pose[6])
       
        # ***lamp settings*** #
        # distance_param is distance between lamp and area
        distance_param = 0.30 # m
        # spot_angle is the angle of the cone of the emitted light in rad
        spot_angle = 10 * pi / 180
        # spot_diameter is the area covered by the light of the lamp
        spot_diameter = (distance_param / cos(spot_angle / 2) * sin(spot_angle)) / sin((pi-spot_angle) / 2) 

        # calculate the angle of inclination between the surface and the reference axis
        BC = [self.pointB[0] - self.pointC[0], self.pointB[1] - self.pointC[1], self.pointB[2] - self.pointC[2]]
        # to check the angle_between_vectors function please refer to "vector utils.py"
        angle_between_vectors(self, repere, BC)
        # p is the angle between BC vector and z axis
        if abs(self.pitch) == 0:
            p = pi/2
        if abs(self.pitch) == pi/2:
            p = 0
        else:
            p = abs(self.pitch - pi/2)
        
        # self.x_val and self.z_val are the values necessary to maintain 
        # the predetermined distance "distance_param" between the lamp and the surface 
        # whatever the direction of the surface
        self.x_val = abs(distance_param * sin(p))
        self.z_val = abs(distance_param * cos(p))

        # compute BC_length which will be used in the calculation of the number of trajectories
        # if the plan is not horizonal then 
        if abs(self.pointA[2] - self.pointD[2])!= 0:
            # in case if the plan is tilted 
            if abs(self.pointA[0] - self.pointD[0]) != 0:
                #BC_length = self.pointA[0] - self.pointD[0] / cos(p)
                x = self.pointA[0] - self.pointD[0]
                BC_length = x/cos(p)
                print("###############################################")
                print("****** TILTED PLAN ******")
                print("###############################################")
            # in case if the plan is vertical 
            else:
                BC_length = self.pointA[2] - self.pointD[2]
                print("###############################################")
                print("****** VERTICAL PLAN ******")
                print("###############################################")
        # in case if the plan is horizonal then BC_length = C[0] - B[0]
        if abs(self.pointA[2] - self.pointD[2]) == 0 and abs(self.pointA[0] - self.pointD[0]) != 0:
            BC_length = abs(self.pointA[0] - self.pointD[0])
            print("###############################################")
            print("****** HORIZONTAL PLAN ******")
            print("###############################################")
        
        # compute number of trajectories needed to scan entire area
        if BC_length != 0 and BC_length <= spot_diameter:
            self.trajectory_nbrs = 1
        if BC_length > spot_diameter:
            # we add 1 because the numbers after the decimal point are not taken into account. 
            # and to be sure that the surface will be scanned entirely 
            self.trajectory_nbrs = int(BC_length / spot_diameter) + 1
        
        # self.xStep_val and self.zStep_val are the values we will add for each iteration
        step_val = BC_length / self.trajectory_nbrs
        self.xStep_val = step_val * cos(p)
        self.zStep_val = step_val * sin(p)
        
        # publish results
        print("###############################################")
        print('normal vector : {0}'.format(self.n_vector))
        print('pitch angle btwn BC and z axis : {0}'.format(p))
        print('total trajectory_nbrs : {0}'.format(self.trajectory_nbrs))
        print('step_val : {0}'.format(step_val))
        print("###############################################")

        self.trajectory_compute()

    def target_Pose(self, Tx, Ty, Tz, Rx, Ry, Rz, Rw):
        #print "in target pose"
        self.target_pose.pose.position.x = Tx
        self.target_pose.pose.position.y = Ty
        self.target_pose.pose.position.z = Tz
        self.target_pose.pose.orientation.x = Rx
        self.target_pose.pose.orientation.y = Ry
        self.target_pose.pose.orientation.z = Rz
        self.target_pose.pose.orientation.w = Rw

        # Set the start state to the current state
        self.arm.set_start_state_to_current_state()
        # Set the goal pose of the end effector to the stored pose
        self.arm.set_pose_target(self.target_pose, self.end_effector_link)        
        # Plan the trajectory to the goal
        traj = self.arm.plan()
        # Execute the planned trajectory
        new_traj = scale_trajectory_speed(traj, 1.0)
        self.arm.execute(new_traj)
        rospy.sleep(1)

    def trajectory_compute(self):   
        for i in range(self.trajectory_nbrs):
            # we will calculate the coordinates of the starting and arrival
            # points that will be used in each new trajectory
            if i == 0:
                # starting pose
                x1_start = abs((self.pointC[0] + (self.xStep_val/2)) - self.x_val)
                y_start = self.pointC[1]
                z1_start = abs((self.pointC[2] + (self.zStep_val/2)) + self.z_val)
                # arrival pose
                x1_arrival = abs((self.pointD[0] + (self.xStep_val/2)) - self.x_val)
                y_arrival = self.pointD[1]
                z1_arrival = abs((self.pointD[2] + (self.zStep_val/2)) + self.z_val)
                print("###############################################")
                print('execute {0} trajectory'.format(i + 1))
                print('start position : {0}'.format([x1_start, y_start, z1_start]))
                print('arrival position : {0}'.format([x1_arrival, y_arrival, z1_arrival]))
                print("###############################################")
                self.target_Pose(x1_start, y_start, z1_start, 
                    self.quaternion[0], self.quaternion[1], self.quaternion[2], self.quaternion[3]) 
                self.target_Pose(x1_arrival, y_arrival, z1_arrival, 
                    self.quaternion[0], self.quaternion[1], self.quaternion[2], self.quaternion[3])
            else:
                # starting pose
                xn_start = abs((x1_start + self.xStep_val))
                y_start = self.pointC[1]
                zn_start = abs((z1_start + self.zStep_val))
                # arrival pose
                xn_arrival = abs((x1_arrival + self.xStep_val))
                y_arrival = self.pointD[1]
                zn_arrival = abs((z1_arrival + self.zStep_val))
                print("###############################################")
                print('execute {0} trajectory'.format(i + 1))
                print('start position : {0}'.format([xn_start, y_start, zn_start]))
                print('arrival position : {0}'.format([xn_arrival, y_arrival, zn_arrival]))
                self.target_Pose(xn_start, y_start, zn_start, 
                    self.quaternion[0], self.quaternion[1], self.quaternion[2], self.quaternion[3]) 
                self.target_Pose(xn_arrival, y_arrival, zn_arrival, 
                    self.quaternion[0], self.quaternion[1], self.quaternion[2], self.quaternion[3])
                self.xStep_val += self.xStep_val
                self.zStep_val += self.zStep_val
                print('self.xStep_val : {0}'.format(self.xStep_val))
                print('self.zStep_val : {0}'.format(self.zStep_val))
                print("###############################################")

        print("Finish executing")
        current_pose = self.arm.get_current_pose(self.end_effector_link).pose
        euler = euler_from_quaternion(quaternion=(current_pose.orientation.x, current_pose.orientation.y, current_pose.orientation.z, current_pose.orientation.w))
        
        # publish end effector pose
        # print("###############################################")
        # print "end effector pose :" + str(current_pose.position)
        # print "Roll : " + str(euler[0])
        # print "Pitch : " + str(euler[1])
        # print "Yaw : " + str(euler[2])
        # print("###############################################")

        # Shut down MoveIt cleanly
        moveit_commander.roscpp_shutdown()
        # Exit MoveIt
        moveit_commander.os._exit(0)

if __name__ == "__main__":
    MoveItDemo()

